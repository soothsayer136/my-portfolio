import React, { Component } from 'react';
import PortfolioComponents from './PortfolioComponents'

class DisplayComponents extends Component {
    render() {
        return (
            <div>
                {this.props.data.map((userInfo) =>{
                  return <PortfolioComponents user={userInfo}/>
                })}
            </div>
        );
    }
}

export default DisplayComponents;