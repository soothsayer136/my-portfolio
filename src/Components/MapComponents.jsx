import React, { Component } from 'react';
import DisplayComponents from './DisplayComponents';
import Image from '../Image/pic.jpg'

class MapComponents extends Component {
    state ={
        myArray:[{
            name: 'Bikram Katuwal',
            email: 'bikrum.katwal@gmail.com',
            phone: 9849867726,
            personalInformation: `A fresh, diligent and highly intelligent bi-lingual student always eager to learn as much knowledge and insight as possible in pursuance of my job and passion. I’m 
            seeking for a reputable commercial law firm that can offer open days where I may 
            shadow experienced legal experts to learn about the day-to-day operation and 
            protocols within professional practice. And will be able to work on own initiative or 
            part of a team and can deal with duties competent.`,
            experience:"Ruby On Rails Intern",
            image: Image
            

        }]
    }
     showInfo(){
         return <DisplayComponents
         data={this.state.myArray} />
     }


    render() {
        return (
            <div>
                <h1>Portfolio</h1>
                {this.showInfo()}
            </div>
        );
    }
}

export default MapComponents;