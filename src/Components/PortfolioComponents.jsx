import React, { Component } from 'react';
// import Background from '../Image/1.jpg'
import { ExternalLink } from 'react-external-link';

class PortfolioComponents extends Component {
    render() {
        return (
            <div style={{ marginLeft:100,display:"flex",  textAlign:"left", backgroundColor:"#2e2e2e"}}>
                <div style={{flexDirection:"column", marginLeft:70, marginTop:50}}>
                <div><img src={this.props.user.image} width='600' height='400'  />  </div> 
                
                <div>
                <div style={{ marginTop:0, width:600}}>
                {/* <p style={{ fontFamily: 'Roboto', fontSize:40}}>Hello</p> */}
                <p style={{ fontFamily: 'Roboto', fontSize:40, color:"white"}}>{this.props.user.name}</p>
                <p style={{fontFamily: 'Roboto', fontSize:20, marginTop:-20, color:"#665C50"}}>{this.props.user.email}</p>
                <p style={{fontFamily: 'Roboto', fontSize:15, marginTop:0, color:"#665C50"}}>{this.props.user.phone}</p>
                <p style={{fontFamily: 'Open-Sans', fontSize:20, fontWeight:500, color:"white" }}>{this.props.user.personalInformation}</p>
                
                <p style={{fontFamily: 'Open-Sans', fontSize:17, fontWeight:600, color:"white"}}>Github: <ExternalLink href="https://gitlab.com/soothsayer136"/></p>
                <p style={{fontFamily: 'Open-Sans', fontSize:17, fontWeight:600, color:"white" }}>Facebook: <ExternalLink href="https://www.facebook.com/deathvalley136"/></p>
                <p style={{fontFamily: 'Open-Sans', fontSize:17, fontWeight:600 }}>Gmail:  bikrum.katwal@gmail.com</p>
                </div>
                </div>
                </div>
                <div style={{paddingLeft:50,  fontFamily: 'Roboto', marginTop:20}}>
                    <p style={{ fontSize:25, color:"white"}}>Experience</p>
                    <ul style={{color:"white"}}>
                        <li>Ruby on Rails Intern</li>
                        <li>CSR in Call Centre</li>
                        <li>Node JS Project</li>
                        <li>Build Wordpress sites: <ExternalLink href="https://www.ecstasyeducation.com"/><br/> <ExternalLink href="https://www.euroasia.com"/> </li>

                    </ul>
                    <p style={{ fontSize:20, color:"white"}}>GitLab Project Listing</p>
                    <ul style={{fontFamily: 'Roboto', fontSize:15, fontWeight:600, color:"white"}}>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/react-fetch-axios"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/react-fetch-fakestore-api-axios"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/react-form-map-delete"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/react-map-delete"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/rojil.shrestha23/react-map-example-app"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/student-form-map-react"/></li>
                        <li>  <ExternalLink href="https://gitlab.com/soothsayer136/student-form-map-react"/></li>
                    </ul>
                </div>
                
            </div>
        );
    }
}

export default PortfolioComponents;