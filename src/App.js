
import './App.css';
import MapComponents from './Components/MapComponents';

function App() {
  return (
    <div className="App">
        <MapComponents/>
    </div>
  );
}

export default App;
